# Welcome to DoubleGDP React Coding Challenge

## What is this project?

This a web application that consumes the ruby on rails api to help you create and update a task.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting Started

- Nodejs. Download and install version 12.16.3 or higher [NodeJs](https://nodejs.org/en/).

- Yarn. Download and install yarn. [Yarn](https://classic.yarnpkg.com).

## Available Scripts

In the project directory, you can run:

#### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

#### `yarn test`

Launches the test runner in the interactive watch mode.<br />

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
