import React from "react";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { formatDate } from "../../Helpers/utils";

const TaskRow = ({ avatar, description, id, checked, updated_at, onCheck }) => {
  return (
    <Box pt={1} pb={3}>
      <Grid container alignItems="center">
        <Grid item xs={2} sm={2}>
          <Box pl={2}>
            <Avatar alt="Remy Sharp" src={avatar} data-testid="avatar" />
          </Box>
        </Grid>
        <Grid item xs={8} sm={8}>
          <Box pl={2}>
            <Typography variant="subtitle1" align="left">
              {description}
            </Typography>
          </Box>
        </Grid>

        <Grid item xs={2} sm={2}>
          {checked ? (
            <Typography variant="caption" align="right">
              {formatDate(updated_at)}
            </Typography>
          ) : (
            <Typography variant="caption" align="right">
              <FormControlLabel
                control={
                  <Checkbox
                    inputProps={{ "data-testid": "checkBox" }}
                    name="checkedC"
                    onChange={() => onCheck(id)}
                  />
                }
              />
            </Typography>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};

export default TaskRow;
