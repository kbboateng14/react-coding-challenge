import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import TaskAdd from "./../../Components/Task/TaskAdd";

describe("TaskAdd Component", () => {
  it("should render all fields", () => {
    render(<TaskAdd />);
    expect(screen.getByTestId(/description/)).toBeInTheDocument();
    expect(screen.getByTestId(/avatar/)).toBeInTheDocument();
    expect(screen.getByTestId(/submit/)).toBeInTheDocument();
    expect(screen.getByTestId(/submit/)).toBeDisabled();
  });
});
