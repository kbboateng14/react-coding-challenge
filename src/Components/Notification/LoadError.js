import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

const LoadError = () => {
  return (
    <Box pt={5} pb={3}>
      <Grid container alignItems="center">
        <Grid item xs={12} sm={12}>
          <Box pl={2}>
            <Typography variant="subtitle1" align="center">
              Sorry Something went wrong.
            </Typography>
            <Typography variant="subtitle2" align="center">
              Please check your internet connection
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default LoadError;
