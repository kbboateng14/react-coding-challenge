import React from "react";
import { Link, useLocation } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import Grid from "@material-ui/core/Grid";
import MenuIcon from "@material-ui/icons/Menu";
import Box from "@material-ui/core/Box";
import "./NavigationBar.css";

const NavigationBar = () => {
  const location = useLocation();

  return (
    <div className="App-header">
      <AppBar position="absolute">
        <Toolbar variant="regular">
          <Grid container spacing={3}>
            <Grid item xs={2} sm={2}>
              <IconButton edge="start" color="inherit" aria-label="menu">
                <MenuIcon />
              </IconButton>
            </Grid>
            <Grid item xs={8} sm={8}>
              <Box pt={1}>
                <Typography variant="h6" align="left" noWrap>
                  <Link to="/" className="link">
                    Tasks
                  </Link>
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={2} sm={2}>
              {location.pathname !== "/new" && (
                <IconButton aria-label="add" color="inherit" data-testid="add">
                  <Link to="/new" className="link">
                    <AddIcon />
                  </Link>
                </IconButton>
              )}
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default NavigationBar;
