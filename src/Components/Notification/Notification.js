import React from "react";
import Snackbar from "@material-ui/core/Snackbar";

export default function NotificationDialog({
  open,
  message = "Task was successfully added",
  handleClose,
}) {
  return (
    <Snackbar
      open={open}
      autoHideDuration={2000}
      onClose={handleClose}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      message={message}
    />
  );
}
