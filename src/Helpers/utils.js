export const formatDate = (date) =>
  new Date(date).toLocaleTimeString([], { timeStyle: "short" });

export const isEmpty = (obj) => Object.keys(obj).length === 0;

export const sortBy = (data = []) =>
  data.sort((a, b) => new Date(b.updated_at) - new Date(a.updated_at));
