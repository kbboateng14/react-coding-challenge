import axios from "./axois";
import { sortBy } from "./utils";

export const getTasks = async (setTasks, setLoadError) => {
  try {
    const { data } = await axios.get(`/tasks`);
    setTasks(data);
  } catch (error) {
    setLoadError(true);
  }
};

export const updateTask = async (id, tasks, setTasks, setOpen, setMessage) => {
  try {
    const { data } = await axios.patch(`/tasks/${id}`, {
      checked: true,
    });
    const updatedTasks = tasks.map((task) =>
      task.id === id ? { ...data } : task
    );
    setTasks(sortBy(updatedTasks));
    setOpen(true);
    setMessage("Task was successfully updated");
  } catch (error) {
    setOpen(true);
    setMessage(
      "Something went wrong. Please check your internet connection and try again."
    );
  }
};
