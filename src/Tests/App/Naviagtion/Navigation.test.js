import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import App from "./../../../Components/App/App";

describe("NavigationBar Component", () => {
  it("should render Tasks title in the navigation bar", () => {
    render(<App />);
    expect(screen.getByText(/Tasks/)).toBeInTheDocument();
  });
});
