import React, { useEffect, useState, Fragment } from "react";
import { getTasks, updateTask } from "../../Helpers/request";
import TaskRow from "./TaskRow";
import LoadError from "./../Notification/LoadError";

const TaskList = ({ setOpen, setMessage }) => {
  const [tasks, setTasks] = useState([]);
  const [loadError, setLoadError] = useState(false);

  useEffect(() => {
    getTasks(setTasks, setLoadError);
  }, []);

  const handleSelectTask = async (id) =>
    await updateTask(id, tasks, setTasks, setOpen, setMessage);

  return (
    <Fragment>
      {tasks.map((task) => (
        <TaskRow
          data-testid="taskRecord"
          onCheck={() => {
            handleSelectTask(task.id);
          }}
          key={task.id}
          {...task}
        />
      ))}
      {loadError && <LoadError />}
    </Fragment>
  );
};

export default TaskList;
