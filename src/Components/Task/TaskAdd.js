import React, { useState, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import axios from "../../Helpers/axois";
import { isEmpty } from "../../Helpers/utils";

const NewTask = ({ history, setOpen, setMessage }) => {
  const [values, setValues] = useState({});

  const handleChange = ({ target }) =>
    setValues({ ...values, [target.name]: target.value });

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.post(`/tasks/`, values);
      setValues({});
      history.push("/");
      setOpen(true);
      setMessage("Task was successfully added");
    } catch (error) {
      setOpen(true);
      setMessage(
        "Something went wrong. Please check your internet connection and try again."
      );
    }
  };

  return (
    <Fragment>
      <form
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
        data-testid="form"
      >
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justify="center"
          style={{ minHeight: "60vh" }}
        >
          <Grid className="align-items-xs-center" item xs={12} sm={12}>
            <Box width="100%">
              <TextField
                label="Task Description"
                name="description"
                inputProps={{ "data-testid": "description" }}
                value={values.description || ""}
                error={!isEmpty(values) && !values.description}
                onChange={handleChange}
                autoFocus
              />
            </Box>
          </Grid>
          <Grid className="align-items-xs-center" item xs={12} sm={12}>
            <TextField
              name="avatar"
              label="Avatar URL"
              inputProps={{ "data-testid": "avatar" }}
              value={values.avatar || ""}
              error={!isEmpty(values) && !values.avatar}
              type="url"
              pattern="https?://.+"
              onChange={handleChange}
              required
            />
          </Grid>
          <Grid className="align-items-xs-center" item xs={12} sm={12}>
            <Box pt={3}>
              <Button
                variant="contained"
                data-testid="submit"
                color="primary"
                type="submit"
                disabled={!values.description || !values.avatar}
              >
                Add
              </Button>
            </Box>
          </Grid>
        </Grid>
      </form>
    </Fragment>
  );
};

export default NewTask;
