import React, { useState, Fragment } from "react";
import { Route, BrowserRouter as Router } from "react-router-dom";
import NotificationDialog from "../Notification/Notification";
import TaskList from "../Task/TaskList";
import NewTask from "../Task/TaskAdd";
import NavigationBar from "../Navigation/NavigationBar";

const Routes = () => {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const closeNotification = () => setOpen(false);
  return (
    <Fragment>
      <Router>
        <div>
          <NavigationBar />
          <Route
            exact
            path="/"
            render={(props) => (
              <TaskList {...props} setOpen={setOpen} setMessage={setMessage} />
            )}
          />
          <Route
            exact
            path="/new"
            render={(props) => (
              <NewTask {...props} setOpen={setOpen} setMessage={setMessage} />
            )}
          />
        </div>
      </Router>
      <NotificationDialog
        open={open}
        handleClose={closeNotification}
        message={message}
      />
    </Fragment>
  );
};

export default Routes;
