import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import LoadError from "../../Components/Notification/LoadError";

describe("LoadError Component", () => {
  it("should render message when user is not connected to the internet", () => {
    render(<LoadError />);
    expect(screen.getByText(/Sorry Something went wrong./)).toBeInTheDocument();
    expect(
      screen.getByText(/Please check your internet connection/)
    ).toBeInTheDocument();
  });
});
