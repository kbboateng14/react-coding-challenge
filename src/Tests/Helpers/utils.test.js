import * as utils from "./../../Helpers/utils";

describe("Utility functions", () => {
  const data = [
    {
      description: "Dummy text one",
      avatar:
        "https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg",
      checked: false,
      updated_at: "2020-08-16 18:17:26.114206",
    },
    {
      description: "Dummy text two",
      checked: false,
      avatar:
        "https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg",
      updated_at: "2020-08-16 18:40:26.114206",
    },
    {
      description: "Dummy text three",
      avatar:
        "https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg",
      checked: false,
      updated_at: "2020-08-16 18:50:26.114206",
    },
  ];
  it("should format the date ......", () => {
    expect(utils.formatDate("2020-08-16 18:17:26.114206")).toBe("6:17 PM");
  });

  it("should sort an array of task according to updatedAt", () => {
    expect(utils.sortBy(data)[0].updated_at).toBe("2020-08-16 18:50:26.114206");
    expect(utils.sortBy(data)[1].updated_at).toBe("2020-08-16 18:40:26.114206");
  });
});
