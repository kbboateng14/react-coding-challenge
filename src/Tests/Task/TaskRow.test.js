import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import TaskRow from "./../../Components/Task/TaskRow";
import { formatDate } from "../../Helpers/utils";

describe("TaskRow Component", () => {
  const initialProps = {
    avatar:
      "https://pbs.twimg.com/profile_images/1276169974051155974/ufFfLGD6_400x400.jpg",
    description: "Something",
    id: 1,
    checked: false,
    onCheck: jest.fn(),
  };

  it("should render all props when passed", () => {
    render(<TaskRow {...initialProps} />);
    expect(screen.getByText(/Something/)).toBeInTheDocument();
    expect(screen.getByTestId(/checkBox/)).toBeInTheDocument();
    expect(screen.getByTestId(/avatar/)).toBeInTheDocument();
  });

  it("should call the onCheck function with the id when checkbox is clicked ", () => {
    render(<TaskRow {...initialProps} />);
    fireEvent.click(screen.getByTestId(/checkBox/));
    expect(initialProps.onCheck).toBeCalledWith(initialProps.id);
  });

  it("should render checkbox component when checked props is false", () => {
    render(<TaskRow {...initialProps} />);
    expect(screen.getByTestId(/checkBox/)).toBeInTheDocument();
  });

  it("should show the time of the task if task is checked", () => {
    const props = {
      ...initialProps,
      checked: true,
      updated_at: "2020-08-08 23:22:13",
    };
    render(<TaskRow {...props} />);
    expect(screen.getByText(formatDate(props.updated_at))).toBeInTheDocument();
  });
});
